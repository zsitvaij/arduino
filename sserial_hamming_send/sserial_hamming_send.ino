// -*- coding:utf-8; mode: c++ -*-
#include <SoftwareSerial.h>
SoftwareSerial mySerial(10, 11); // RX, TX
//SoftwareSerial mySerial(11, 10); // RX, TX
void setup() {
  Serial.begin(115200);
  mySerial.begin(4800);
}

byte prev = 0;
long received = -1;
long bad = 0;
long sent = 0;

const byte msg[] = {
  0b00000000,
  0b11010010,
  0b01010101,
  0b10000111,
  0b10011001,
  0b01001011,
  0b11001100,
  0b00011110,
  0b11100001,
  0b00110011, 
  0b10110100,
  0b01100110,
  0b01111000,
  0b10101010,
  0b00101101,
  0b11111111
};

byte dec(byte y) {
  byte a = (y & 0b00001110) >> 1;
  byte b = (y & 0b00100000) >> 2;
  return a|b;
}

byte recover(byte y) {
  byte p1 = (y & 0b10000000) >> 7;
  byte p2 = (y & 0b01000000) >> 6;
  byte d1 = (y & 0b00100000) >> 5;
  byte p3 = (y & 0b00010000) >> 4;
  byte d2 = (y & 0b00001000) >> 3;
  byte d3 = (y & 0b00000100) >> 2;
  byte d4 = (y & 0b00000010) >> 1;
  byte p4 = (y & 0b00000001);
  
  byte pp1 = (p1 + d1 + d2 + d4) % 2;
  byte pp2 = (p2 + d1 + d3 + d4) % 2;
  byte pp3 = (p3 + d2 + d3 + d4) % 2;
  byte pp4 = (p1 + p2 + d1 + p3 + d2 + d3 + d4) % 2;
  
  byte corr;
  if (pp1 & pp2 & pp3) {
    corr = ((~d4 & 1) << 1) | (~0b00000010 & y);
  } else if (pp1 & pp2) {
    corr = ((~d1 & 1) << 5) | (~0b00100000 & y);
  } else if (pp1 & pp3) {
    corr = ((~d2 & 1) << 3) | (~0b00001000 & y);
  } else if (pp2 & pp3) {
    corr = ((~d3 & 1) << 2) | (~0b00000100 & y);
  } else {
    corr = msg[dec(y)];
  }
  return corr;
}

void is_valid(byte y) {
  byte rec = dec(y);
  if (msg[rec] != y) {
    bad++;
    rec = dec(recover(y));
  }
  prev = rec;
  received++;
}

void send_hamming() {
    for (byte i = 0; i < 16; i++) {
      mySerial.write(msg[i]);  
    }
  sent += 16;
}

boolean done = false;
boolean start = false;
void loop()
{
  if (mySerial.available()) {
    start = true;
    Serial.println("start");
  }
  if (!done && start) {
//    if (sent < 65535) {
      if (sent < 6553) {
      send_hamming();
      Serial.println(sent);
    } else {
      delay(1000);
      Serial.println("sent");
      Serial.println(sent);
      done = true;
    }
  }
  //delay(15);
}
